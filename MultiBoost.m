%
% MultiBoost - A stage-wise implementation of multi-class boosting
%
% ARGUMENTS:
% trn training samples 
%  .X  input   [dim x num]
%  .y  output  [  1, num ] (label must be from 1..K)
% options.T - max. number of boosting iterations (default is 100. Range: [1 inf])
% options.regparam - regularization parameter (something small ~ 1e-10. Range: [0 inf])
% options.shrinkparam - shrinkage parameter (default is 0.5. Range: [0 1])
% 
%
% Note: weak learner currently output {-1, +1}
%
% The algorithm uses Quasi-Newton method, L-BFGS-B, to solve
% the primal problem (step 4 in Algorithm 1). A MATLAB interface for L-BFGS-B 
% can be downloaded from: http://www.cs.ubc.ca/~pcarbo/lbfgsb-for-matlab.html
%
% Copyright (c) S. Paisitkriangkrai, C. Shen and A. van den Hengel
% The University of Adelaide
% contact: chunhua.shen(at)adelaide.edu.au
% September 2012.
%
%


function model = MultiBoost(trn, options)

DEFAULT_MAXITER = 100;    
DEFAULT_REGPARAM = 1e-8;
DEFAULT_SHRINKPARAM = 0.5;

rng('default');

if ~isfield(options, 'T')
    maxiter = DEFAULT_MAXITER;
else
    maxiter = options.T;
end
    
if ~isfield(options, 'regparam')
    regparam = DEFAULT_REGPARAM;
else
    regparam = options.regparam;
end

if ~isfield(options, 'shrinkparam')
    shrinkparam = DEFAULT_SHRINKPARAM;
else
    shrinkparam = options.shrinkparam;
    if shrinkparam <= 0 || shrinkparam > 1
        shrinkparam = DEFAULT_SHRINKPARAM;
    end
end


% ========================================================================
%
% Initialize
%
% ========================================================================

model.id = 'MultiBoost';
model.alpha = [];
model.weakc = [];
model.tags = [];

label = trn.y(:);
num_data = length(label);
data = trn.X';    
Y = unique(label);
C = length(Y);
data_W = ones(C, num_data) / (C*num_data);
delta_Mat = zeros(C, num_data);
for i = 1 : num_data,
    delta_Mat(label(i), i) = 1;
end
conv_accu = 1e-10;

SumUir = zeros(num_data, num_data * C);
for i = 1 : num_data,
    zero_Mat = zeros(C, num_data);
    zero_Mat(:, i) = 1;
    SumUir(i, :) = (zero_Mat(:))';
end


model.TagMat = {};
[sorted_data, sorted_index] = sort(data, 1);

% ========================================================================
%
% Training
%
% ========================================================================

t = 0;
U_ir = data_W;
H = zeros(C, num_data);
alpha_Mat = [];

while true    
    t = t + 1;
    new_W = delta_Mat .* (ones(C, 1) * sum(data_W)) - data_W;
    [weakc tags r] = weak_learner_train(data, sorted_data, sorted_index, label, new_W);    
        
    cg_constr = new_W(r, :) * tags(:);
    % Stopping criteria
    if cg_constr <=  conv_accu
        fprintf('\n\nThe program converges. Training will now stop\n\n');
        return;
    end
    
    model.weakc(t,:) = weakc;
    model.tags(t,:) = tags;

    % ---------------------------------------------------------------
    % Solve the optimization problem using L-BFGS-B
    % ---------------------------------------------------------------        
    lb = zeros(C, 1);
    ub =  ones(C, 1) * Inf;
    alpha_start = rand(C, 1);
    aux_data{1} = [num_data, C, t, regparam];
    aux_data{2} = U_ir;
    Tags_Mat(t, :) = tags(:)';
    aux_data{3} = Tags_Mat;
    aux_data{4} = delta_Mat';
    alpha_opt = lbfgsb(alpha_start(:), lb, ub, ...
            'MultiBoostBFGSObj','MultiBoostBFGSGrad', aux_data);
    % apply shrinkage factor
    alpha_opt = shrinkparam*alpha_opt;
    alpha_opt = reshape(alpha_opt, C, 1);
    alpha_Mat = [alpha_Mat alpha_opt];
    model.alpha = alpha_Mat;
        
    % ---------------------------------------------------------------
    % Update sample weights
    % ---------------------------------------------------------------   
    H = H + alpha_Mat(:,t) * model.tags(t,:);
    HiWyi = sum(delta_Mat .* H);
    Rho = ones(C, 1) * HiWyi - H;
    U_ir = exp(-1 * Rho);
    data_W = U_ir ./ (num_data*C*(1 + U_ir));
    data_W = data_W ./ sum(data_W(:));

    if mod(t,50) == 0, fprintf('%d\n', t);    
    else fprintf('.'); end
    
    % Stopping criteria
    if (t >= maxiter)
        break;
    end

end


% ========================================================================
%
% Return trained model
%
% ========================================================================
6
model.attr_num  = length(unique(label));
model.categs = unique(label);
model.fun = 'weak_learner_eval';
model.iter_num = length(model.alpha);
model.iteration_end = t;


end

% 
% sorted_x - projected weak learner (sorted) [ num x classes, D ]
% idx_x - index of the sorted data
% labels - class label
% weights - sample weights
%
function [stump, tags m_idx] = weak_learner_train(data, sorted_data, sorted_index, label, data_w); 

K = size(data_w, 1);
M = zeros(K, 1);
stump_pool = cell(K, 1);
tags_pool = cell(K, 1);

label1 = ones(length(label), 1);
for r = 1 : K,    
    [parity, sel_feat, thresh] = weak_learner_helper(...
                    sorted_data, sorted_index, label1, -data_w(r,:)');   
    W = zeros(1, size(sorted_data,2)); W(sel_feat) = parity;                
    stump_pool{r} = [W thresh];
    tags_pool{r} = weak_learner_eval(W*data', thresh);
    M(r) = data_w(r, :) * tags_pool{r}(:);
end
m_idx = find(M == max(M));
m_idx = m_idx(1);
stump = stump_pool{m_idx};
tags = tags_pool{m_idx};

end


function [parity, sel_feat, thresh] = weak_learner_helper(sorted_x, idx_x, labels, weights)
[~, dim] = size(sorted_x);

b = zeros(dim,1);
parities = zeros(dim,1);
Errors = zeros(dim,1);

for j = 1:dim
    sorted_x_j = sorted_x(:,j);
    idx_x_j = idx_x(:,j);
    u_j = weights(idx_x_j);
    y_j = labels(idx_x_j);
    
    Sp = zeros(1, length(sorted_x_j));
    Sn = zeros(1, length(sorted_x_j));
    Sp(y_j==1) = u_j(y_j==1);
    Sn(y_j==-1) = u_j(y_j==-1);
    Sp = cumsum(Sp);
    Sn = cumsum(Sn);
    Tp = Sp(end);
    Tn = Sn(end);
    err = (Sp+Tn-Sn);
    
    x1 = sorted_x_j(1:end-1);
    x2 = sorted_x_j(2:end);
    dup = [double(x1==x2); 0];
    dup_idx = find(dup==1);
    err1=err;
    err2=Tp+Tn-err;
    err1(dup_idx) = 1.0;
    err2(dup_idx) = 1.0;
    
    [minerr1,inx1]= min(err1);
    [minerr2,inx2] = min(err2); 
    
    if minerr1 < minerr2
        parities(j) = -1;
        Errors(j) = minerr1;
        if inx1 < length(sorted_x_j), b(j) = -(sorted_x_j(inx1)+sorted_x_j(inx1+1))*0.5; else b(j)=-(sorted_x_j(inx1)+1); end
    else
        parities(j) = 1;
        Errors(j) = minerr2;
        if inx2 < length(sorted_x_j), b(j) = (sorted_x_j(inx2)+sorted_x_j(inx2+1))*0.5; else b(j)=(sorted_x_j(inx2)+1); end
    end
end

[~,inx] = min(Errors);
parity = parities(inx);
sel_feat = inx;
thresh = b(inx);


end


% EOF





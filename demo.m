% ========================================================================
%
% This MATLAB script demonstrates the decision boundary of the MultiBosot
% on toy data sets.
% See http://arxiv.org/abs/1307.5497  for details.
% ========================================================================
function demo

addpath('./lbfgsb-for-matlab');

rng('default');

% PARAMETERS TOY DATA:
Nsamples = 1000;
Nclasses = 5;

% PARAMETERS FOR MULTIBOOST
options.T = 50; % maximum number of iterations
options.regparam = 1e-9; % regularization parameter
options.shrinkparam = 0.5; % shrinkage parameter

% Create toy data
samples = rand(2, Nsamples);
centers = rand(2, Nclasses-1);
D = zeros(Nclasses-1, Nsamples);
for c = 1:Nclasses-1
    D(c,:) = sum((samples - repmat(centers(:,c), [1 Nsamples])).^2);
end
[d, cl] = min(D);
cl = cl.*(d<.05);
cl(find(cl==0)) = max(cl)+1; % bg class
theta = linspace(0,pi,60); theta = theta(1:end-1);
features=[];
for i = 1:length(theta)
    features(i,:) = cos(theta(i))*(samples(1,:)-.5) + sin(theta(i))*(samples(2,:)-.5);
end
trn.X = features;
trn.y = cl;

% LEARNING CLASSIFIER
model = MultiBoost(trn, options);

% SHOW THE CLASSIFIER
tst_point_x = 0:.01:1;
tst_point_y = 0:.01:1;
[xt,yt] = meshgrid(tst_point_x, tst_point_y); [ncols nrows] = size(xt);
xt = xt(:); yt = yt(:);
theta = linspace(0,pi,60); theta = theta(1:end-1);
featurestest = [];
for i = 1:length(theta)
    featurestest(i,:) = cos(theta(i))*(xt-.5) + sin(theta(i))*(yt-.5);
end

% run classifier and plot the decision boundary
tst.X = featurestest;
[Cx, Fx] = MultiBoost_eval(tst, model);

figure('name', 'class boundary'); axis('square')
colors = 'crgbkmy';
plotoptions.categ = length(unique(trn.y));
plotoptions.gridx = 100;
plotoptions.gridy = 100;
plotoptions.fill  = 1;
plotoptions.smooth = 1;
plot_boundary(Cx, tst_point_x, tst_point_y, plotoptions);
hold on;
for c = 1:Nclasses
    j = find(cl==c);
    plot(samples(1,j), samples(2,j), 'o', 'color', colors(c));
    hold on
end
hold off;

end


% --------------------------------------------------------------------
function h = plot_boundary(L, X_pos, Y_pos, options)
% --------------------------------------------------------------------
    K = options.categ;

    % Plots decision boudary.
    dx = X_pos(2) - X_pos(1);
    dy = Y_pos(2) - Y_pos(1);
    m = length(X_pos);
    n = length(Y_pos);
    Z = NaN * ones(m + 2, n + 2);

    mask = fspecial('gauss',[5 5],1); % fspecial is from images toolbox

    A = reshape(L', n, m)';
    Z(2 : end - 1, 2 : end - 1) = A;

    Z(2 : end - 1, 2 : end - 1) = filter2(mask, A);
    contour_handle = @contourf;

    [~, h] = contour_handle([X_pos(1) - dx,X_pos,X_pos(end)+dx], ...
        [Y_pos(1)-dy,Y_pos,Y_pos(end)+dy], Z', K-1);

    % color setting
    colormap(gray(K) * 0.4 + 0.6);
    axis([X_pos(1), X_pos(end), Y_pos(1), Y_pos(end)]);

    clear('A');
    return;

end




% Gradient function of MultiBoost (Logistic)
%
%
function grad_val = MultiBoostBFGSGrad(omega, aux_data)

parameters  = aux_data{1};
num = parameters(1);
C   = parameters(2);
T   = parameters(3);
regparam = parameters(4);
U_ir = aux_data{2};
Tags_Mat    = aux_data{3};
delta_Mat   = aux_data{4}; 


alpha_Mat = reshape(omega, C, 1);

HiWr = alpha_Mat * Tags_Mat(end, :);
HiWy = sum( (delta_Mat .* HiWr'), 2 );
w_gama = ones(C, 1) * HiWy' - HiWr;
grad_val = zeros(C, 1);
log_val =  logistic_loss_grad(U_ir, w_gama);
for c = 1 : C,
        Hit = ones(C, 1) * Tags_Mat(end, :);
        delta_yic = ones(C, 1) * delta_Mat(:, c)';
        delta_lc = zeros(C, num);
        delta_lc(c, :) = 1;
        tmp = log_val .* (delta_yic - delta_lc);
        grad_val(c, :) = sum(sum(  Hit .* tmp  ));
end
grad_val = grad_val + regparam;
grad_val = grad_val(:);

end


function ret = logistic_loss_grad(U_ir, rho_ij)
    tmp = exp(-rho_ij);
    ret = -(U_ir .* (tmp) ./ (1+(U_ir.*tmp)));
end

%
% Decision stump classifier
%
function h = weak_learner_eval(X, thresh)

h = sign(X - thresh);

end

% Object function of MultiBoost (Logistic)
%
%
function obj_val = MultiBoostBFGSObj(omega, aux_data)

parameters  = aux_data{1};
num = parameters(1);
C   = parameters(2);
T   = parameters(3);
regparam = parameters(4);
U_ir = aux_data{2};
Tags_Mat    = aux_data{3};
delta_Mat   = aux_data{4}; 

alpha_Mat = reshape(omega, C, 1);
HiWr = alpha_Mat * Tags_Mat(end,:);
HiWy = sum( (delta_Mat .* HiWr'), 2 );
w_gama = ones(C, 1) * HiWy' - HiWr;
obj_val = sum(sum( log( 1+(U_ir.*exp(- w_gama)) ) ));
obj_val = obj_val + (regparam * sum(omega));

end




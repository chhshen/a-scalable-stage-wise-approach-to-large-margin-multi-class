A scalable stage-wise approach to large-margin multi-class loss based boosting
===========================================================================

[Click to download the code as a zip file](https://bitbucket.org/chhshen/a-scalable-stage-wise-approach-to-large-margin-multi-class/get/master.zip)


This program implements the method described in:

```
S. Paisitkriangkrai, C. Shen and A. van den Hengel,
A scalable stage-wise approach to large-margin multi-class loss based boosting.
```
See the paper at: <http://arxiv.org/abs/1307.5497>


The paper proposes simple and scalable boosting algorithms based on
a stage-wise boosting method:

The algorithm uses Quasi-Newton method, L-BFGS-B, to solve
the primal problem (step 4 in Algorithm 1). A MATLAB interface for L-BFGS-B
can be downloaded from:


A MATLAB interface for L-BFGS-B
by Peter Carbonetto
Dept. of Computer Science, University of British Columbia
http://www.cs.ubc.ca/~pcarbo/lbfgsb-for-matlab.html
This work is licensed under a Creative Commons
Attribution-Noncommercial-Share Alike 2.5 Canada License.

We have included pre-compiled versions for Linux (64bit) and Mac OSX
(64bit). Tested on Mac OSX 10.7 Lion.


SETUP
===========================================================================

Modify the file demo.m to include the path to L-BFGS-B matlab code

DEMO
===========================================================================

demo.m - generate 2D artificial data sets and use MultiBoost
to train multi-class classifier. Decision boundary will be plotted at the
end of the training process.

REFERENCES
===========================================================================


If you use this code in your research work, please cite the following paper:

```
 @article{Paul2013Fastboosting,
   author    = "S. Paisitkriangkrai and  C. Shen and  A. {van den Hengel}",
   title     = "A scalable stage-wise approach to large-margin multi-class loss based boosting",
   journal   = "IEEE Transactions on Neural Networks and Learning Systems",
   year      = "2014",
 }
```


COPYRIGHT
=================

Copyright (c) S. Paisitkriangkrai, C. Shen and A. van den Hengel

University of Adelaide

contact: chunhua.shen@adelaide.edu.au

September 2012.


This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but __WITHOUT ANY
WARRANTY__; without even the implied warranty of __MERCHANTABILITY__ or __FITNESS FOR
A PARTICULAR PURPOSE__. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.




%
% Arguments:
%  tst training samples 
%   .X  - input   [dim x Nsamples]
%   model - learned using MultiBoost
%
% Retruns:
%  predict_label - class prediction
%  Z - class responses [Nclass x Nsamples]
%
function [predict_label, Z] = MultiBoost_eval(tst, model)

data = tst.X;
C = model.attr_num;
T = model.iter_num;
[~, num] = size(data);
Z = zeros(C, num);
alpha_Mat = model.alpha(:,1:T);
fn = str2func(model.fun);

for tp = 1 : T,
    Mat = model.weakc(tp, :);
    W = Mat(1 : end - 1);
    b = Mat(end);
    H = feval(fn, W*data, b);
    Z = Z + alpha_Mat(:, tp) * H;
end

[max_value predict_label] = max(Z);
predict_label = predict_label(:);
end

